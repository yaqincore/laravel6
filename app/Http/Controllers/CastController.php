<?php

namespace App\Http\Controllers;

use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as IlluminateRequest;

class CastController extends Controller
{
    //
    public function index()
    {
        # code...
        $loopdatacast = DB::table('cast')->get();
        return view('cast.index', compact('loopdatacast'));
    }

    public function create()
    {
        # code...
        return view('cast.fcreate');
    }

    public function store(Request $data)
    {
        # code...
        DB::table('cast')->insert(
            [
                'nama' => $data['nama'],
                'umur' => $data['umur'],
                'bio' => $data['bio']
            ]
        );

        return redirect('/cast');
        // dd($data->all());
    }

    public function show($id)
    {
        # code...
        $datacast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('datacast'));
    }

    public function edit($id)
    {
        # code...
        $editcast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('editcast'));
    }

    public function update(Request $data, $id)
    {
        # code...
        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $data['nama'],
                    'umur' => $data['umur'],
                    'bio' => $data['bio']
                ]
            );
        return redirect('/cast');
    }

    public function destroy($id)
    {
        # code...
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
