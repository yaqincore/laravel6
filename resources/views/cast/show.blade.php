@extends('layout.main')

@section('judul1')
Details
@endsection

@section('judul2')
Details
@endsection

@section('data')

<p class="h4 mb-4">Nama : {{$datacast->nama}}</p>
<p class="h4 mb-4">Umur : {{$datacast->umur}}</p>
<p class="h5">Biodata :</p>
<p class="card-text mb-5">{{$datacast->bio}}</p>

<a href="/cast" class="link-primary">Kembali</a>
<!-- <a href="#" class="card-link">Card link</a>
<a href="#" class="card-link">Another link</a> -->

@endsection