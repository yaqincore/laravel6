@extends('layout.main')

@section('judul1')
Edit Data
@endsection

@section('judul2')
Edit Data
@endsection

@section('data')
<div class="col-6 p-0 mb-4">
    <form action="/cast/{{$editcast->id}}" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input name="nama" class="form-control" type="text" placeholder="Nama" id="nama" required value="{{$editcast->nama}}">
        </div>
        <div class="mb-3">
            <label for="umur" class="form-label">Umur</label>
            <input name="umur" class="form-control" type="number" placeholder="Umur" id="umur" required value="{{$editcast->umur}}">
        </div>
        <div class="mb-3">
            <label for="bio" class="form-label">Bio</label>
            <textarea name="bio" class="form-control" placeholder="Biodata..." id="bio" style="height: 150px" required>{{$editcast->bio}}</textarea>
        </div>
        <button name="submit" type="submit" class="btn btn-primary">simpan</button>
    </form>
</div>
<a href="/cast" class="link-primary">Kembali</a>
@endsection