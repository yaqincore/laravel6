@extends('layout.main')

@section('judul1')
Data Cast
@endsection

@section('judul2')
Data Cast
@endsection

@section('data')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah Data</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Unur</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($loopdatacast as $no => $data)
        <tr>
            <th scope="row">{{$no + 1}}</th>
            <td>{{$data->nama}}</td>
            <td>{{$data->umur}}</td>
            <td><a href="/cast/{{$data->id}}" class="link-primary">Details</a></td>
            <td>
                <a href="/cast/{{$data->id}}/edit" class="btn btn-sm btn-outline-primary">Edit</a> |
                <form action="/cast/{{$data->id}}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" name="delete" value="delete" class="btn btn-sm btn-outline-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>


@endsection